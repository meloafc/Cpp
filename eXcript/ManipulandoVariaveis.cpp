#include <iostream>

using namespace std;

int main() {
	int inteiro = 100;
	char caracter = 'r';
	double pontoFlutuante = 5.99;
	
	cout << "O valor da variavel inteiro e: " << inteiro << endl;
	cout << "O valor da variavel caracter e: " << caracter << endl;
	cout << "O valor da variavel pontoFlutuante e: " << pontoFlutuante << endl;
	cout << endl;
	cout << "Memoria da variavel inteiro: " << sizeof(inteiro) << " bytes" << endl;
	cout << "Memoria da variavel caracter: " << sizeof(caracter) << " bytes" << endl;
	cout << "Memoria da variavel pontoFlutuante: " << sizeof(pontoFlutuante) << " bytes" << endl;
	
	return 0;
}
