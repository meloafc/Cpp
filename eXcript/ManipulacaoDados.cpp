#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {

    // (<<) operador de insercao
    // (>>) operador de extracao

    int numero1, numero2;
    cout << "Digite um numero: " << endl;
    cin >> numero1;

    cout << "Digite outro numero: " << endl;
    cin >> numero2;

    cout << "Os numeros digitados foram: "
            << numero1 << " e " << numero2 << endl;

    system("pause");

    return 0;
}
