#include <iostream>
#include <stdlib.h>

using namespace std;

int main() {

    int a = 0; // 4 bytes
    short int b = 0; // 2 bytes
    long int c = 0; // tamanho pode variar dependendo da arquitetura

    cout << "Tamanhos:" << endl;
    cout << sizeof(a) << endl;
    cout << sizeof(b) << endl;
    cout << sizeof(c) << endl;

    int a2 = 0;
    signed int b2 = -10;
    unsigned int c2 = -10; // nao salva numeros negativos

    cout << "Valores:" << endl;
    cout << a2 << endl;
    cout << b2 << endl;
    cout << c2 << endl;

    char caractere = 127;
    unsigned char caractere2 = -127;

    cout << "Caracteres:" << endl;
    cout << caractere << endl;
    cout << caractere2 << endl;

    system("pause");
    return 0;
}
